# DB DESCRIPTION

```sql
CREATE TABLE logs (
    id mediumint(9) NOT NULL AUTO_INCREMENT,
    time_of_logging datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
    telegram_user varchar(30) NOT NULL,
    progress tinyint(3),
    comment varchar(255),
    rowtype tinyint(1) NOT NULL,
    PRIMARY KEY  (id)
)

CREATE TABLE dayoff (
    id mediumint(9) NOT NULL AUTO_INCREMENT,
    time_of_logging datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
    telegram_user varchar(30) NOT NULL,
    daytype tinyint(1) NOT NULL,
    PRIMARY KEY  (id)
)

CREATE TABLE books (
    id mediumint(9) NOT NULL AUTO_INCREMENT,
    telegram_user varchar(30) NOT NULL,
    pages mediumint(5) NOT NULL,
    book_name varchar(255) NOT NULL,
    PRIMARY KEY  (id)
);
```

