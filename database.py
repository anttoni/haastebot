import mysql.connector
from keys import *

class Database: 
    ## Get a mysql connection
    def connect():
        db_connection = mysql.connector.connect(
            host="localhost",
            user="paljubot",
            passwd=mysql_pass, ## Imported from keys.py
            database="palju"
        )
        return db_connection

    def read( table, row, where=False ):
        connection = Database.connect()
        if ( not connection.is_connected() ):
            return False
        db = connection.cursor()
        sql_statement = "SELECT " + row + " from " + table
        if (where):
            sql_statement += " WHERE " + where
        db.execute(sql_statement)
        rows = db.fetchall()
        return rows

    
    def write( table, statement, values ):
        connection = Database.connect()
        if ( not connection.is_connected() ):
            return False
 
        db = connection.cursor()
        sql_statement = "INSERT INTO " + table + " " + statement
        db.execute(sql_statement, values)
        connection.commit()
 